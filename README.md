# jtx Board

## Get the most out of journals, notes & tasks
Elevate the power of the ical-standard to the next level,
use the potential of the combination of journals, notes and tasks out of one app
and use DAVx5 to synchronize your entries with the server of your choice!

### iCal standard compliant
Using the iCal standard ensures compatibility and interoperability with other apps and services independent of a dedicated provider or infrastructure.

### Combine journals, notes & tasks
Instead of using separate apps for journals, notes & tasks you can use them out of one hand, combine and link them to each other, e.g. create meeting minutes and link your tasks to them. 

### Sync with DAVx5 (coming soon)
Synchronize your entries with any compatible CalDAV server by using DAVx5 (https://www.davx5.com/). By using DAVx5 you are free to choose your preferred provider for CalDAV, you can even use your local server to store and synchronize your data.

**Find out more on https://jtx.techbee.at/**

---

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"
    alt="Get it on Google Play"
    height="80">](https://play.google.com/store/apps/details?id=at.techbee.jtx)
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/de/packages/at.techbee.jtx/)

---



### Contributing

Contributions are always welcome! Whether financial support, translations, code changes, bug reports, feature requests, or otherwise, your help is appreciated. For more information please have a look at [Contribute](https://jtx.techbee.at/app/contribute) on our website.

[![PayPal donate button](https://img.shields.io/badge/paypal-donate-yellow.svg?logo=paypal)](https://www.paypal.com/donate/?hosted_button_id=KNCKKUUYN4FMJ)


### Communication

For communication with the team and other people, please use the forums to get in touch: [Forums](https://forums.bitfire.at/category/30/jtx-board)

<img src="https://jtx.techbee.at/wp-content/uploads/2021/12/header-image.png" alt="jtx Board Banner with Screenshot" width="70%">


### Flavors

jtx Board provides different flavors: 
- gplay is the flavor for the Google Play store that contains ads from Admob and the billing manager for monetization (subscription to remove ads). 
- huawei is the flavor for the Huawei App Gallery that also contains ads, but from the Huawei Ads provider. 
- global is another flavor that contains ads from Admob but no billing manager.
- ose is the open source edition. **If you would like to create build the app from source, this is the recommended flavor.** This flavor contains only open source libraries and no ads. Instead of ads this flavor has an additional page for donations visible.

Currently there is also no difference in functionalities between the flavors. 


### Permissions
jtx Board uses/requests the following permissions:
- GET_ACCOUNTS is used to determine if there are accounts set up in DAVx5 and show them in the UI
- RECORD_AUDIO can be used to access the microphone for adding audio notes
- READ_SYNC_STATS is used to show a progress bar when a synchronization through DAVx5 is currently in progress
- INTERNET is used to retrieve the list of contributors for translations from POEditor.com
